package es.cipfpbatoi.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Tarea;
import es.cipfpbatoi.modelo.repositorios.TareaRepository;

@Controller
public class TareaController {

	@Autowired
	private TareaRepository tareaRepository;

	@GetMapping("/tarea-form")
	public String tareaFormActionView() {
		return "tarea_form_view";
	}

	@GetMapping("/tarea-form-buscar")
	public String tareaBuscarActionView() {
		return "tarea_form_buscar_view";
	}

	@PostMapping(value = "/tarea-add")
	@ResponseBody
	public String postAddAction(@RequestParam Map<String, String> params) {
		int code = Integer.parseInt(params.get("code"));

		String user = params.get("user");

		String desc = params.get("description");

		String priorityValue = params.get("priority");
		Tarea.Prioridad priority = Enum.valueOf(Tarea.Prioridad.class, priorityValue);

		String realizadaValue = params.get("done");
		boolean done = realizadaValue != null ? true : false;

		String dateValue = params.get("date");
		String timeValue = params.get("time");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime dateExpiration = LocalDateTime.parse(dateValue + " " + timeValue, formatter);

		Tarea tarea = new Tarea(code, user, desc, dateExpiration, priority, done);
		tareaRepository.add(tarea);

		return "<html><body><font style='color:red'> Tarea " + tarea.getCodigo() + " creada con éxito</font> <br>"
				+ tarea + "</body></html>";
	}

	@GetMapping("/tarea-find")
	@ResponseBody
	public String getFindAction(@RequestParam int code) {
		try {
			Tarea tarea = tareaRepository.get(code);
			return "<html><body> " + tarea + "</body><html>";
		} catch (NotFoundException e) {
			return "<html><body><font style='color:red'>Tarea " + code + " no encontrada</font></body><html>";
		}
	}	

	@GetMapping("/tarea-delete")
	@ResponseBody
	public String deleteAction(@RequestParam int code) {
		StringBuilder responseHtml = new StringBuilder();
		responseHtml.append("<html><body><font style='color:red'>");
		try {
			Tarea tarea = tareaRepository.get(code);

			tareaRepository.delete(tarea);

			responseHtml.append("Tarea ").append(code).append(" borrada con éxito</font>");
			responseHtml.append("<br>" + tarea);
		} catch (NotFoundException e) {
			responseHtml.append("Tarea ").append(code).append(" no encontrada</font>");
		}
		responseHtml.append("</body><html>");
		return responseHtml.toString();
	}
}
