package es.cipfpbatoi.modelo.repositorios;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Tarea;

@Service
public class TareaRepository {

	private List<Tarea> tareas;

	public TareaRepository() {
		this.tareas = new ArrayList<>();
		Tarea t1 = new Tarea(1, "Sergio", "Corregir", LocalDateTime.now(), Tarea.Prioridad.ALTA, true);
		Tarea t2 = new Tarea(2, "Roberto", "Preparar", LocalDateTime.now(), Tarea.Prioridad.BAJA, false);
		Tarea t3 = new Tarea(3, "Raul", "Instalar", LocalDateTime.now(), Tarea.Prioridad.MEDIA, false);
		this.tareas.add(t1);
		this.tareas.add(t2);
		this.tareas.add(t3);
	}

	/**
	 * Añade la Tarea recibida como argumento a la base de datos en memoria
	 * 
	 * @param tarea
	 */
	public void add(Tarea tarea) {
		this.tareas.add(tarea);
		System.out.println("Añadida con exito!");
		mostrarTareas();
	}
	
	private void mostrarTareas() {
		for(Tarea t: this.tareas) {
			System.out.println("Tarea " + t.getCodigo() + " de " + t.getNombreUsuario() + ": " + t.getDescripcion());
		}
		
	}

	/**
	 * Borra la Tarea con código @codTarea recibida como argumento 
	 * de la base de datos en memoria
	 * 
	 * @param codTarea
	 * @throws NotFoundException 
	 */
	public void delete(int codTarea) throws NotFoundException {
		Tarea tarea = this.get(codTarea);
		this.tareas.remove(tarea);
		System.out.println("Borrada con exito!");
		mostrarTareas();
	}
	
	public void delete(Tarea tarea) throws NotFoundException {		
		this.tareas.remove(tarea);
		System.out.println("Borrada con exito!");
		mostrarTareas();
	}

	/**
	 * Obtiene la Tarea con codigo @codTarea. En caso de que no la encuentre
	 * devolverá una excepción
	 * 
	 * @throws NotFoundException 
	 * @param codTarea
	 */
	public Tarea get(int codTarea) throws NotFoundException {
		for (Tarea t : this.tareas) {
			if (t.getCodigo() == codTarea) {
				return t;
			}
		}
		throw new NotFoundException("La tarea con codigo " + codTarea + " no existe");
	}

	/**
	 * Devuelve el listado de todas las tareas.
	 */
	public List<Tarea> findAll() {
		return this.tareas;		
	}

	/**
	 * Devuelve el listado de todas las tareas cuyo atributo nombre coincide
	 * con @userName
	 */
	public List<Tarea> findAll(String userName) {
		List<Tarea> tareasPorUsuario = new ArrayList<>();
		for(Tarea t: this.tareas) {
			if (t.getNombreUsuario().equals(userName)) {
				tareasPorUsuario.add(t);
			}				
		}
		return tareasPorUsuario;
	}

}
